# Productos

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.1.

## Instalacion

Para realizar la instalacion, debe terner instaldo NodeJs mayor a 16.0. Para el caso de bootstrap ya se tiene importado el CDN

## Instrucciones

Clonar el reporsitorio en su maquina local

```
mkdir Front
cd Front
git clone https://gitlab.com/cesar098/test-front.git

```

Con el proyecto clonado se debe instalar las dependencias. 
En caso de consumir la API-Rest de Laravel desde un host distinto a local, se debe modificar la variable _url de todos los archivos .service
```
npm install

#//config host API-Rest
#//_url = 'http://127.0.0.1:8000'
_url = 'http://hostremote:8000'

```

Como ulltimo iniciar el servidor con el siguiente comando

```

ng serve

```
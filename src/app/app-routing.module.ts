import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { ClienteComponent } from './components/cliente/cliente.component';

import { HomeComponent } from './components/home/home.component';
import { ListVentasComponent } from './components/list-ventas/list-ventas.component';
import { MainComponent } from './components/main/main.component';
import { ProductosComponent } from './components/productos/productos.component';
import { ProveedorComponent } from './components/proveedor/proveedor.component';
import { VendedoresComponent } from './components/vendedores/vendedores.component';
import { VentaComponent } from './components/venta/venta.component';

const routes: Routes = [
  {path: '', redirectTo:'home', pathMatch: 'full'},
  { 
    path: '', 
    data: {
      title: 'main'
    },
    component: MainComponent,
    children: [
      { path: 'venta', component: VentaComponent },
      { path: 'cliente', component: ClienteComponent },
      { path: 'producto', component: ProductosComponent },
      { path: 'proveedor', component: ProveedorComponent },
      { path: 'vendedor', component: VendedoresComponent },
      { path: 'listventa', component: ListVentasComponent },
    ]
  },
  {path: 'home', component: HomeComponent}
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'top',
      anchorScrolling: 'enabled',
      initialNavigation: 'enabledBlocking'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

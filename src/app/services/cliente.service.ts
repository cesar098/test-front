import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  _url = 'http://143.244.190.188:8000/api/clientes'
  constructor(
    private http: HttpClient
  ) { }

  getClientes(){
    let header = new HttpHeaders()
      .set('Type-content', 'aplication/json')

    return this.http.get(this._url, {
      headers: header
    });
  }
  
  addClientes(client: any){
    return this.http.post(this._url, client)
  }

  updateClientes(client: any){
    return this.http.put(this._url.concat('/'+client.id.toString()), client)
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {
  _url = 'http://143.244.190.188:8000/api/proveedores'
  constructor(
    private http: HttpClient
  ) { }

  getProveedor(){
    let header = new HttpHeaders()
      .set('Type-content', 'aplication/json')

    return this.http.get(this._url, {
      headers: header
    });
  }
  
  addProveedor(proveedor: any){
    return this.http.post(this._url, proveedor)
  }

  updateProveedor(proveedor: any){
    return this.http.put(this._url.concat('/'+proveedor.id.toString()), proveedor)
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  _url = 'http://143.244.190.188:8000/api/productos'
  constructor(
    private http: HttpClient
  ) { }

  getProducto(){
    let header = new HttpHeaders()
      .set('Type-content', 'aplication/json')

    return this.http.get(this._url, {
      headers: header
    });
  }
  
  addProducto(producto: any){
    return this.http.post(this._url, producto)
  }

  updateProducto(producto: any){
    return this.http.put(this._url.concat('/'+producto.id.toString()), producto)
  }
}

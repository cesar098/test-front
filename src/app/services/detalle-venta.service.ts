import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DetalleVentaService {
  _url = 'http://143.244.190.188:8000/api/detalleventas'
  constructor(
    private http: HttpClient
  ) { }

  getDetalleVenta(){
    let header = new HttpHeaders()
      .set('Type-content', 'aplication/json')

    return this.http.get(this._url, {
      headers: header
    });
  }
  
  addDetalleVenta(detaelleVenta: any){
    return this.http.post(this._url, detaelleVenta)
  }

  updateDetalleVenta(detaelleVenta: any){
    return this.http.put(this._url.concat('/'+detaelleVenta.id.toString()), detaelleVenta)
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class VendedorService {
  _url = 'http://143.244.190.188:8000/api/vendedores'
  constructor(
    private http: HttpClient
  ) { }

  getVendedores(){
    let header = new HttpHeaders()
      .set('Type-content', 'aplication/json')

    return this.http.get(this._url, {
      headers: header
    });
  }
  
  addVendedores(vendedor: any){
    return this.http.post(this._url, vendedor)
  }

  updateVendedores(vendedor: any){
    return this.http.put(this._url.concat('/'+vendedor.id.toString()), vendedor)
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Venta } from '../models/Venta';

@Injectable({
  providedIn: 'root'
})
export class VentaService {
  _url = 'http://143.244.190.188:8000/api/ventas'
  constructor(
    private http: HttpClient
  ) { }

  getVenta(){
    let header = new HttpHeaders()
      .set('Type-content', 'aplication/json')

    return this.http.get<Venta>(this._url, {
      headers: header
    });
  }
  
  addVenta(venta: any){
    return this.http.post<Venta>(this._url, venta)
  }

  updateVenta(venta: any){
    return this.http.put(this._url.concat('/'+venta.id.toString()), venta)
  }
}

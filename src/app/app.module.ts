import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { Content1Component } from './components/content1/content1.component';
import { Content2Component } from './components/content2/content2.component';
import { AppRoutingModule } from './app-routing.module';
import { MainComponent } from './components/main/main.component';
import { VentaComponent } from './components/venta/venta.component';
import { SiderbarComponent } from './components/siderbar/siderbar.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ProductosComponent } from './components/productos/productos.component';
import { ProveedorComponent } from './components/proveedor/proveedor.component';
import { VendedoresComponent } from './components/vendedores/vendedores.component';
import { ListVentasComponent } from './components/list-ventas/list-ventas.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Content1Component,
    Content2Component,
    MainComponent,
    VentaComponent,
    SiderbarComponent,
    ClienteComponent,
    ProductosComponent,
    ProveedorComponent,
    VendedoresComponent,
    ListVentasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

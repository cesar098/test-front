import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { DetalleVentaService } from 'src/app/services/detalle-venta.service';
import { ProductoService } from 'src/app/services/producto.service';
import { VendedorService } from 'src/app/services/vendedor.service';
import { VentaService } from 'src/app/services/venta.service';
import { DetalleVenta } from '../../models/DetalleVenta'
import { Producto } from '../../models/Producto'
import { Venta } from '../../models/Venta'
import { Vendedor } from '../../models/Vendedor'



@Component({
  selector: 'app-list-ventas',
  templateUrl: './list-ventas.component.html',
  styleUrls: ['./list-ventas.component.css']
})
export class ListVentasComponent implements OnInit {

  public ventas: Array<any> = []
  public vendedores: Array<any> = []
  public clientes: Array<any> = []
  public detalleVentas: Array<any> = []
  public detalleVenta: Array<any> = []
  venta = {
    id: '',
    vendedor: '',
    id_cliente: '',
    cliente: '',
    fecha: '',
    total: ''
  }
  
  constructor(
    private ventaService: VentaService,
    private vendedorService: VendedorService,
    private clienteService: ClienteService,
    private productoSevice: ProductoService,
    private detalleVentaSerivice: DetalleVentaService
  ) { }
  listVenta(){
    this.ventaService.getVenta().subscribe((respV: any) => {
      this.ventas = respV
    })
    this.vendedorService.getVendedores().subscribe((respVn: any) => {
        this.vendedores = respVn
    })
    this.clienteService.getClientes().subscribe((respC: any) => {
      this.clientes = respC
    })
  }
  getNameCliente(id_cliente: number){
    var cliente = this.clientes.find(cliente=>cliente.id===id_cliente)
    return cliente.nombre
  }
  getNameVendedor(id_vendedor: number){
    var vendedor = this.vendedores.find(vendedor=>vendedor.id===id_vendedor)
    return vendedor.nombre
  }
  listDetalleVenta(){
    this.detalleVentaSerivice.getDetalleVenta().subscribe((respDV: any) => {
      this.productoSevice.getProducto().subscribe((respP: any) => {
        this.detalleVentas = respDV.map((t1: DetalleVenta) => ({...t1, ...respP.find((t2: Producto) => t2.id === t1.id_producto)}))
      })
    })
  }

  ngOnInit(): void {
    this.listVenta()
    this.listDetalleVenta()
  }
  

  loadData(detail: any){
    this.venta = detail
    this.venta.cliente = this.getNameCliente(detail.id_cliente)
    this.venta.vendedor = this.getNameVendedor(detail.id_vendedor)
    this.detalleVenta = this.detalleVentas.filter(detalle=>detalle.id_venta===this.venta.id)
    console.log(this.detalleVenta)
  }
}

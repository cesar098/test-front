import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  public clientes: Array<any> = []

  newClient = {
    nombre: '',
    apellidos: '',
    rut: '',
    telefono: '',
    direccion: ''
  }
  detailClient = {
    id: '',
    nombre: '',
    apellidos: '',
    rut: '',
    telefono: '',
    direccion: ''
  }
  alert = true
  
  constructor(
    private clientService: ClienteService
  ) { 
    this.listClient()
  }
  listClient(){
    this.clientService.getClientes().subscribe((resp: any) => {
      this.clientes = resp
    })
  }

  ngOnInit(): void {
  }
  
  guardar(){    
    if(this.newClient.nombre.length>0&&this.newClient.apellidos.length>0&&parseInt(this.newClient.rut)>0&&parseInt(this.newClient.telefono)>0&&this.newClient.direccion.length>0){
      this.clientService.addClientes(this.newClient)
      .subscribe(data=>{
        console.log(data)
        window.location.href = '/cliente';
      })
    }else{
      this.alert =false
    }
    this.listClient()
  }

  loadData(detail: any){
    this.detailClient = detail
  }
  editClient(){
    this.clientService.updateClientes(this.detailClient)
    .subscribe(data=>{
      console.log(data)
    })
  }

}

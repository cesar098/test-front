import { Component, OnInit } from '@angular/core';
import { VendedorService } from 'src/app/services/vendedor.service';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.component.html',
  styleUrls: ['./vendedores.component.css']
})
export class VendedoresComponent implements OnInit {
  public vendedores: Array<any> = []

  newVendedor = {
    nombre: '',
    apellidos: '',
    rut: '',
    telefono: '',
    direccion: '',
    fecha_nac: '',
    email: ''
  }
  detailVendedor = {
    id: '',
    nombre: '',
    apellidos: '',
    rut: '',
    telefono: '',
    direccion: '',
    fecha_nac: '',
    email: ''
  }
  alert = true
  
  constructor(
    private vendedorService: VendedorService
  ) { 
    this.listVendedores()
  }
  listVendedores(){
    this.vendedorService.getVendedores().subscribe((resp: any) => {
      this.vendedores = resp
    })
  }

  ngOnInit(): void {
  }
  
  guardar(){    
    if(this.newVendedor.nombre.length>0&&this.newVendedor.apellidos.length>0&&parseInt(this.newVendedor.rut)>0&&parseInt(this.newVendedor.telefono)>0&&this.newVendedor.fecha_nac.length>0&&this.newVendedor.email.length>0){
      this.vendedorService.addVendedores(this.newVendedor)
      .subscribe(data=>{
        console.log(data)
        window.location.href = '/vendedor';
      })
    }else{
      this.alert = false
    }
  }

  loadData(detail: any){
    this.detailVendedor = detail
  }
  editVendedor(){
    this.vendedorService.updateVendedores(this.detailVendedor)
    .subscribe(data=>{
      console.log(data)
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { DetalleVentaService } from 'src/app/services/detalle-venta.service';
import { ProductoService } from 'src/app/services/producto.service';
import { VendedorService } from 'src/app/services/vendedor.service';
import { VentaService } from 'src/app/services/venta.service';
import { detailVenta } from '../../models/DetailVentas'


@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit {
  public clientes: Array<any> = []
  public vendedores: Array<any> = []
  public productos: Array<any> = []
  public detailVentas: Array<detailVenta> = []

  detailClient = {
    id: '',
    nombre: '',
    apellidos: '',
    rut: '',
    telefono: '',
    direccion: ''
  }
  detailVenta = {
    id_venta: 0,
    id_producto: 0,
    nombre: '',
    cantidad: 0,
    precio: 0,
    subtotal: 0
  }
  venta = {
    id_vendedor: 1,
    id_cliente: 0,
    fecha: '',
    total: 0
  }
  selected = ''
  id_vendedor = 0
  totalVenta = 0
  alert = true

  constructor(
    private clientService: ClienteService,
    private productoService: ProductoService,
    private ventaService: VentaService,
    private detalleVetaService: DetalleVentaService,
    private vendedorService: VendedorService 
  ) {
    this.listClient()
    this.listProducto()
    this.listVendedor()
  }

  ngOnInit(): void {
  }

  editStock(product: any){
    product.stock = product.stock - 1
  }
  
  save(){
    if(this.detailVentas.length>0 && parseInt(this.detailClient.id) >0 && this.id_vendedor>0){
      let date = new Date()
      this.venta.id_vendedor = this.id_vendedor
      this.venta.id_cliente = parseInt(this.detailClient.id)
      this.venta.fecha = date.toISOString().split('T')[0]
      this.venta.total = this.totalVenta
      this.ventaService.addVenta(this.venta)
      .subscribe(data=>{
        this.detailVentas.forEach(detail => {
          detail.id_venta = data.id
          this.saveDetalleVenta(detail)
          this.updateProducto(detail.id_producto)
        });
      })
      window.location.href = '/venta';
    }else{
      this.alert=false
    }
    
  }

  updateProducto(_id: number){
    var producto = this.productos.find(producto=>producto.id===_id)
    this.productoService.updateProducto(producto)
    .subscribe(data=>{
      return data
    })
  }

  saveDetalleVenta(detail: any){
    this.detalleVetaService.addDetalleVenta(detail)
    .subscribe(data=>{
      return data
    })
  }

  calcTotal(){
    var total = 0
    this.detailVentas.forEach(numero => total = total + numero.subtotal);
    this.totalVenta = total
  }

  addProduct(product: any){
    this.alert = true
    this.editStock(product)
    const indice = this.detailVentas.findIndex(producto=>producto.id_producto===product.id)
    if(indice == -1){
      this.detailVenta.id_producto=product.id
      this.detailVenta.nombre=product.nombre
      this.detailVenta.cantidad=1
      this.detailVenta.precio=product.precio
      this.detailVenta.subtotal=product.precio
      var newobj = Object.assign({}, this.detailVenta); //new code
      this.detailVentas.push(newobj)
    }else{
      const cantidad: number = this.detailVentas[indice].cantidad
      this.detailVentas[indice].cantidad = cantidad + 1
      this.detailVentas[indice].subtotal = (cantidad + 1) * this.detailVentas[indice].precio
    }    
    this.calcTotal()
  }

  deleteProducto(product: any){
    const indice = this.productos.findIndex(producto=>producto.id===product.id_producto)
    this.productos[indice].stock += product.cantidad
    const indiceProducto = this.detailVentas.findIndex(producto=>producto.id_producto===product.id_producto)
    this.detailVentas.splice(indiceProducto, 1)
    this.calcTotal()
  }

  listClient(){
    this.clientService.getClientes().subscribe((resp: any) => {
      this.clientes = resp
    })
  }
  listVendedor(){
    this.vendedorService.getVendedores().subscribe((resp: any) => {
      this.vendedores = resp
    })
  }
  listProducto(){
    this.productoService.getProducto().subscribe((resp: any) => {
      this.productos = resp
    })
  }

  selectOffice(value: any){
    this.selected = value;
  }
  selectVendedor(value: any){
    this.id_vendedor = value;
    this.alert = true
  }

  loadDataClient(detail: any){
    this.detailClient = detail
    this.alert = true
  }

}

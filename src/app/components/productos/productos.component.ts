import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/services/producto.service';
import { ProveedorService } from 'src/app/services/proveedor.service';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
  public productos: Array<any> = []
  public proveedors: Array<any> = []

  newProduct = {
    id_proveedor: '',
    nombre: '',
    precio: '',
    stock: ''
  }
  detailProduct = {
    id_proveedor: '',
    nombre: '',
    precio: '',
    stock: ''
  }
  alert = true
  

  constructor(
    private proveedorService: ProveedorService,
    private productoService: ProductoService,
  ) { 
    this.listProducto()
    this.listProveedor()
  }
  listProducto(){
    this.productoService.getProducto().subscribe((resp: any) => {
      this.productos = resp
    })
  }
  listProveedor(){
    this.proveedorService.getProveedor().subscribe((resp: any) => {
      this.proveedors = resp
    })
  }

  selectProveedor(value: any){
    this.newProduct.id_proveedor = value;
  }

  ngOnInit(): void {
  }
  
  guardar(){  
    if(this.newProduct.nombre.length>0&&parseInt(this.newProduct.precio)>0&&parseInt(this.newProduct.stock)>0){
      this.productoService.addProducto(this.newProduct)
      .subscribe(data=>{
        console.log(data)
        window.location.href = '/producto';
      })
    }else{
      this.alert = false
    }
  }

  loadData(detail: any){
    this.detailProduct = detail
  }

  editProducto(){
    this.productoService.updateProducto(this.detailProduct)
    .subscribe(data=>{
      console.log(data)
    })
  }

}

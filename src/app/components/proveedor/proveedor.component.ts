import { Component, OnInit } from '@angular/core';
import { ProveedorService } from 'src/app/services/proveedor.service';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css']
})
export class ProveedorComponent implements OnInit {
  public proveedores: Array<any> = []

  newProveedor = {
    nombre: '',
    rut: '',
    direccion: '',
    pagina_web: ''
  }
  detailProveedor = {
    nombre: '',
    rut: '',
    direccion: '',
    pagina_web: ''
  }
  alert = true

  constructor(
    private proveedorService: ProveedorService
  ) { 
    this.listProveedor()
  }
  listProveedor(){
    this.proveedorService.getProveedor().subscribe((resp: any) => {
      this.proveedores = resp
    })
  }

  ngOnInit(): void {
  }
  
  guardar(){    
    if(this.newProveedor.nombre.length>0&&parseInt(this.newProveedor.rut)>0&&this.newProveedor.direccion.length>0&&this.newProveedor.pagina_web.length>0){
      this.proveedorService.addProveedor(this.newProveedor)
      .subscribe(data=>{
        console.log(data)
        window.location.href = '/proveedor';
      })
    }else{
      this.alert = false
    }
  }

  loadData(detail: any){
    this.detailProveedor = detail
  }
  editProveedor(){
    this.proveedorService.updateProveedor(this.detailProveedor)
    .subscribe(data=>{
      console.log(data)
    })
  }

}

export interface cliente {
    id: number;
    nombre: string;
    apellidos: string;
    rut: number;
    telefono: number;
    direccion: string
}
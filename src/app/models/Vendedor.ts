export interface Vendedor {
    id: number;
    nombre: string;
    apellidos: string;
    rut: number;
    telefono: number;
    direccion: string;
    fecha_nac: string;
    email: string
}
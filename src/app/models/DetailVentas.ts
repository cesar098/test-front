export interface detailVenta {
    id_venta: number;
    id_producto: number;
    nombre: string;
    cantidad: number;
    precio: number;
    subtotal: number
}
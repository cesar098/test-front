export interface Producto {
    id: number;
    id_proveedor: number;
    nombre: string;
    precio: number;
    stock: number;
}
export interface Venta {
    id: number;
    id_vendedor: number;
    id_cliente: number;
    fecha: string;
    total: number
}
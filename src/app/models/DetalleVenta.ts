export interface DetalleVenta {
    id: number;
    id_venta: number;
    id_producto: number;
    cantidad: number;
    subtotal: number
}